package com.git.hugovallada.controller;

import com.git.hugovallada.response.AnimalResponse;
import com.git.hugovallada.usecase.GetAnimalsUseCase;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import lombok.AllArgsConstructor;

import java.util.List;

@Controller("/animals")
@AllArgsConstructor
public class AnimalController {

    private final GetAnimalsUseCase getAnimalsUseCase;

    @Get
    public HttpResponse<List<AnimalResponse>> getAllAnimals() {
        return HttpResponse.ok(getAnimalsUseCase.execute());
    }

}
