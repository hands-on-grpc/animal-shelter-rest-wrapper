package com.git.hugovallada.usecase;

import com.git.hugovallada.gateway.GetAnimalGateway;
import com.git.hugovallada.response.AnimalResponse;
import jakarta.inject.Singleton;
import lombok.AllArgsConstructor;

import java.util.List;

@Singleton
@AllArgsConstructor
public class GetAnimalsUseCase {

    private final GetAnimalGateway gateway;

    public List<AnimalResponse> execute() {
        return gateway.execute();
    }

}
