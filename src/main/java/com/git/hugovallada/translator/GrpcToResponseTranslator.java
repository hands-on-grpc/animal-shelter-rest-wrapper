package com.git.hugovallada.translator;

import com.git.hugovallada.AnimalResponseGrpc;
import com.git.hugovallada.response.AnimalResponse;

import java.util.List;
import java.util.stream.Collectors;

public class GrpcToResponseTranslator {

    public static AnimalResponse translate(AnimalResponseGrpc responseGrpc) {
        return new AnimalResponse(
                responseGrpc.getId(), responseGrpc.getBreed(), responseGrpc.getAge(),
                responseGrpc.getTemperament(), responseGrpc.getAvailableForAdoption(), responseGrpc.getPreviousAdopted(),
                responseGrpc.getIdTag()
        );
    }

    public static List<AnimalResponse> translate(List<AnimalResponseGrpc> responseListGrpc) {
        return responseListGrpc.stream().map(GrpcToResponseTranslator::translate).collect(Collectors.toList());
    }

}
