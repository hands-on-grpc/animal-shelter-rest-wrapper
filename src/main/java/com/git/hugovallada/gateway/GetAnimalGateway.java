package com.git.hugovallada.gateway;

import com.git.hugovallada.GetAnimalsServiceGrpc;
import com.git.hugovallada.response.AnimalResponse;
import com.git.hugovallada.translator.GrpcToResponseTranslator;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import java.util.List;

@Singleton
public class GetAnimalGateway {

    @Inject
    private GetAnimalsServiceGrpc.GetAnimalsServiceBlockingStub grpcClient;

    public List<AnimalResponse> execute() {

        var response = grpcClient.findAll(null);

        return GrpcToResponseTranslator.translate(response.getAnimalsList());

    }
}
