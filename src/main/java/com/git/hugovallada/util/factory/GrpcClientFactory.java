package com.git.hugovallada.util.factory;

import com.git.hugovallada.GetAnimalsServiceGrpc;
import io.grpc.ManagedChannel;
import io.micronaut.context.annotation.Factory;
import io.micronaut.grpc.annotation.GrpcChannel;
import jakarta.inject.Singleton;
import lombok.AllArgsConstructor;

@Factory
@AllArgsConstructor
public class GrpcClientFactory {

    @Singleton
    public GetAnimalsServiceGrpc.GetAnimalsServiceBlockingStub getAnimals(@GrpcChannel("animal-shelter") ManagedChannel channel) {
        return GetAnimalsServiceGrpc.newBlockingStub(channel);
    }
}
