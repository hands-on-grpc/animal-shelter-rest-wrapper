package com.git.hugovallada.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class AnimalResponse {

    private final String id;

    private final String breed;

    private final Integer age;

    private final String temperament;

    private final boolean availableForAdoption;

    private final boolean previousAdopted;

    private final String idTag;


}
